package Controle;


import Model.*;
import Model.MySql.MysqlDatabase;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Arthur Reginaldo Ramos
 */
public class TelaPrincipalController extends MysqlDatabase implements Initializable {
  
    /* ---------------------------------------------- */
    // O ID DA TABLE VIEW.
    
    @FXML
    private TextField filtroPesquisa;
    
    @FXML 
    private TableView<Dados> tbViewDados;
    
    @FXML
    private TableColumn<Dados, String> colunaNome;

    @FXML
    private TableColumn<Dados, String> colunaCPF;

    /* ---------------------------------------------- */
    //OS TEXT DA FICHA DO MENU PRINCIPAL.
    
    @FXML
    private Text textTELEFONE;

    @FXML
    private Text textNOME_EMERGENCIA;

    @FXML
    private Text textCPF;

    @FXML
    private Text textTIPOSANGUE;

    @FXML
    private Text textCURSO;

    @FXML
    private Text textENDERECO;

    @FXML
    private Text textTELEFONE_EMERGENCIA;

    @FXML
    private Text textNOME;
    
    @FXML
    private Text textFATORRH;

    
    /* ---------------------------------------------- */
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
       ObservableList<Dados> popularTableView = FXCollections.observableArrayList();
       popularTableView.addAll(Dados.allOpen());
       colunaNome.setCellValueFactory(cellData -> cellData.getValue().getNomeProperty());
       colunaCPF.setCellValueFactory(cellData -> cellData.getValue().getCpfProperty());
  
        FilteredList <Dados> filtrado = new FilteredList<> (popularTableView, p ->true);
        
        
        
        
        filtroPesquisa.textProperty (). addListener ((observable, oldValue, newValue) -> {
			filtrado.setPredicate (Dados -> {
				// Se o texto do filtro estiver vazio, exiba todas as pessoas.
				if (newValue == null || newValue.isEmpty ()) {
					return true;
				}
				
				// Compare o primeiro nome e sobrenome de cada pessoa com o texto do filtro.
				String lowerCaseFilter = newValue.toLowerCase ();
				
				if (Dados.getNome().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filtrar corresponde ao primeiro nome.
				} 
                                else if (Dados.getCpf().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filter matches last name.
				}
				return false; // Não corresponde.
			});
		});
		
		// 3. Enrole o FilteredList em uma SortedList. 
		SortedList<Dados> sortedData = new SortedList<>(filtrado);
		
		// 4. Bind the SortedList comparator to the TableView comparator.
		// 	  Otherwise, sorting the TableView would have no effect.
		sortedData.comparatorProperty().bind(tbViewDados.comparatorProperty());
		
		// 5. Add sorted (and filtered) data to the table.
		tbViewDados.setItems(popularTableView);
        
        
        
        tbViewDados.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> popularFicha(newValue));

    }    
    
    //*POPULAR FICHA É O METODO QUE RECEBER O VALOR DA TABLEVIEW
    //*E POPULA A FICHA DO MENU PRINCIPAL.
    public void popularFicha (Dados dados){
      
        textNOME.setText(dados.getNome());
        /* textENDERECO
        textTELEFONE*/
        textCPF.setText(dados.getCpf());
        /* textTIPOSANGUE
        textFATORRH*/
        textCURSO.setText(dados.getCurso());
        /*textNOME_EMERGENCIA
        textTELEFONE_EMERGENCIA */      
        
    }
    
       public void limparFicha (){
      
        textNOME.setText("");
        textENDERECO.setText("");
        textTELEFONE.setText("");
        textCPF.setText("");
        textTIPOSANGUE.setText("");
        textFATORRH.setText("");
        textCURSO.setText("");
        textNOME_EMERGENCIA.setText("");
        textTELEFONE_EMERGENCIA.setText("");     
        
    }
    
    //*METODO DE AÇÃO TANTO DO BOTÃO DELETAR DO MENU PRINCIPAL
    //*QUANTO DO BOTÃO DELETAR DO MENU SUPERIOR
    //*
    //*ELE VERIFICA SE HÁ ALGUM ITEM SELECIONADO NA TABLE VIEW,
    //*CASO ESTEJA, LINKA COM A CLASSE ABSTRATA DO PACOTE Model,
    //*QUE POR SUA VEZ BUSCA O COMANDO MQL NO PACOTE Model.MySql
    //*PARA A EXECUÇÃO DO COMANDO DELETE NO BANCO DE DADOS
    //*UTILIZANDO O ID DA FICHA SELECIONADAO EM SEGUIDA ATUALIZANDO A TABLEVIEW.
    
    @FXML void aoClicarDeletar(){
        Dados itemSelecionado = tbViewDados.getSelectionModel().getSelectedItem(); 
        
        if(itemSelecionado !=null){
         limparFicha();
         Dados.excluir(itemSelecionado);
         atualizarTableView();
        
            
        }else{System.out.println("Não tem !");}
        
    }
    
    @FXML void aoClicarEditar() throws IOException {  
         Dados itemSelecionado = tbViewDados.getSelectionModel().getSelectedItem(); 
            
         if (itemSelecionado != null){
            limparFicha();
            FXMLLoader loader = new FXMLLoader(getClass().getResource(Patchs.telaEditar));
            Parent editOrder_parent = loader.load();
            TelaEditarController controller = loader.getController();
            controller.setDados(itemSelecionado);
            
            Stage stage = new Stage();
            Scene editOrder_scene = new Scene(editOrder_parent);
            stage.setScene(editOrder_scene);
            stage.setTitle("Tela de Editação");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setResizable(false);
            stage.show();

            //Update Order Table after Order be edited.
             stage.setOnHiding((WindowEvent event1) -> {
             uparTableView();
        });
             
         }else{
             System.out.println("Não há nem um item selecionado na table view!!!!");
         }
    }
    
    @FXML
    void aoClicarAdicionar(ActionEvent event) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource(Patchs.telaAdicionar));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Tela de Inserção");
        stage.setResizable(false);
        stage.show();
        //Update Order Table after Order be edited.
        stage.setOnHiding((WindowEvent event1) -> {
            uparTableView();
        });
    }
  
  
    private void uparTableView() {
        ObservableList<Dados> popularTableView = FXCollections.observableArrayList(Dados.allOpen());
        tbViewDados.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nome"));
        tbViewDados.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("cpf"));
        
        FilteredList <Dados> filtrado = new FilteredList<> (popularTableView, p ->true);
        
        
        
        
        filtroPesquisa.textProperty (). addListener ((observable, oldValue, newValue) -> {
			filtrado.setPredicate (Dados -> {
				// Se o texto do filtro estiver vazio, exiba todas as pessoas.
				if (newValue == null || newValue.isEmpty ()) {
					return true;
				}
				
				// Compare o primeiro nome e sobrenome de cada pessoa com o texto do filtro.
				String lowerCaseFilter = newValue.toLowerCase ();
				
				if (Dados.getNome().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true; // Filtrar corresponde ao primeiro nome.
				} 
				return false; // Não corresponde.
			});
		});
		
		// 3. Enrole o FilteredList em uma SortedList. 
		SortedList<Dados> sortedData = new SortedList<>(filtrado);
		
		// 4. Bind the SortedList comparator to the TableView comparator.
		// 	  Otherwise, sorting the TableView would have no effect.
		sortedData.comparatorProperty().bind(tbViewDados.comparatorProperty());
		
		// 5. Add sorted (and filtered) data to the table.
		tbViewDados.setItems(popularTableView);
       
    }
    
    
    private void atualizarTableView(){
        uparTableView();
    }

}
