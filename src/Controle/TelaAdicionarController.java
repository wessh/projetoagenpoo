
package Controle;

import Model.Dados;
import Model.DadosCurso;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

/**
 *
 * @author Arthur Reginaldo Ramos
 */
public class TelaAdicionarController implements Initializable {

      @FXML 
    private TextField tfCpf;

    @FXML 
    private TextField tfNome;
    
    @FXML
    private Button bEnviar;
    
    
        @FXML
    private ComboBox<DadosCurso> comboCurso;
    
    
    
   public void comboBx (){
       ObservableList<DadosCurso> popularComboBox = FXCollections.observableArrayList(DadosCurso.all());  
       comboCurso.getItems().addAll(popularComboBox);       
   }
    
    
    
    
    @FXML
    public void aoClicarEnviar(){
        String curso = String.valueOf(comboCurso.getValue());
        System.out.println(curso);
                Dados dds = new Dados(tfNome.getText(),tfCpf.getText(), curso);
                
        Dados.adicionar(dds);     
        Stage stage = (Stage) bEnviar.getScene().getWindow();
        stage.hide();     
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        comboBx();

    }    
    
}
