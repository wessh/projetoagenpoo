package Controle;

import Model.Patchs;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Arthur Reginaldo Ramos
 * ERRO no Controle.TelaEditarController. 
 * o erro está localizado o FXML_TelaEditar.fxml
 * está faltando subistituir o textField por um ComboBox
 * e designar um id para tal.
 * ou apenas designar o ID nomeCurso para o textFiel/comboBox. 
 */
public class ControlePrincipal extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Patchs.telaprincipal));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
