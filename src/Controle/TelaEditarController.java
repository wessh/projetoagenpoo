
package Controle;

import Model.Dados;
import Model.DadosCurso;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Arthur Reginaldo Ramos
 */
public class TelaEditarController implements Initializable {

    
    @FXML
    private TextField tfCpf;

    @FXML
    private TextField tfNome;
    
    @FXML
    private Button bEditar;

    private int id;
    
    @FXML
    private ComboBox<DadosCurso> comboBoxCurso;
    
    @FXML
    void aoClicarEditar(ActionEvent event) {
    String curso = String.valueOf(comboBoxCurso.getValue());
        
                Dados dds = new Dados(
                        this.id,
                        tfNome.getText(),
                        tfCpf.getText(),
                        curso);
                
                        
        Dados.editar(dds);
        
        Stage stage = (Stage) bEditar.getScene().getWindow();
        stage.hide();   
    }
    
    
    public void setDados(Dados dados) {
       tfNome.setText(dados.getNome());
       tfCpf.setText(dados.getCpf());
       this.id = dados.getId();
        System.out.println(dados.getId());
    }   
    
       public void comboBx (){
       ObservableList<DadosCurso> popularComboBox = FXCollections.observableArrayList(DadosCurso.all());  
       comboBoxCurso.getItems().addAll(popularComboBox);       
   }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        comboBx();
   
    }   
    

}
