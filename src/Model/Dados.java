
package Model;


import Model.MySql.*;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
        

/**
 *
 * @author Arthur Reginaldo Ramos & Wesley Barros Ribeiro
 */
public class Dados {
    
    public static OrdemDados dao = new DadosMySQL();    
    private StringProperty nome, cpf, curso;
    private IntegerProperty id;

    

    public Dados(int id, String nome, String cpf, String curso) {
        this.nome = new SimpleStringProperty(nome);
        this.cpf = new SimpleStringProperty(cpf);
        this.curso = new SimpleStringProperty(curso);
        this.id = new SimpleIntegerProperty(id);
    }
    
    
    
 public Dados(String nome, String cpf, String curso) {
        this.nome = new SimpleStringProperty(nome);
        this.cpf = new SimpleStringProperty(cpf);
        this.curso = new SimpleStringProperty(curso);
        
    }

    public StringProperty getNomeProperty() {
        return nome;
    }
    
       public String getNome() {
        return nome.get();
    }

    public void setNome(String nome) {
        this.nome.set(nome);
    }

    public String getCpf() {
        return cpf.get();
    }

    public void setCpf(String cpf) {
        this.cpf.set(cpf);
    }
    
    public StringProperty getCpfProperty() {
        return cpf;
    }

    public StringProperty getCursoProperty() {
        return curso;
    }
    
    public String getCurso() {
        return curso.get();
    }

    public void setCurso(String curso) {
        this.curso.set(curso);
    }

    
    public IntegerProperty getIdProperty() {
        return id;
    }
    
    public int getId() {
        return id.get();
    }


    public void setId(int id) {
        this.id.set(id);
    }
    
 
 
    
    
    public static void adicionar (Dados dados){
        dao.adicionar(dados);
    }
    
    public static void editar(Dados dados){
        dao.editar(dados);
    }
    
    public static void excluir(Dados dados){
        dao.excluir(dados);
    }
    
    public static List<Dados> allOpen(){
        return dao.allOpen();
    }
    
    

    
}
