
package Model;

import Model.MySql.CursoMySQL;
import java.util.List;


public class DadosCurso {
    
    
    public static OrdemCurso dao = new CursoMySQL();  
    
    
   private int idCurso;
   private String nomeCurso;
   
   
   
   

    public DadosCurso( String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public DadosCurso(int idCurso, String nomeCurso) {
        this.idCurso = idCurso;
        this.nomeCurso = nomeCurso;
    }

    @Override
    public String toString() {
        return this.nomeCurso; 
    }
 
   
    
    
    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }
    
        public static List<DadosCurso> all(){
        return dao.all();
    }
   
}
