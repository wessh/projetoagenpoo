package Model;

import java.util.List;

/**
 *
 * @author Arthur Reginaldo Ramos
 */
public interface OrdemDados {
    
    void adicionar (Dados dados);
    void editar (Dados dados);
    void excluir (Dados dados);
    
     List<Dados> allOpen();
    
}
