/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.MySql;

import Model.DadosCurso;
import Model.OrdemCurso;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class CursoMySQL  extends MysqlDatabase implements OrdemCurso {

    @Override
    public List<DadosCurso> all() {
     List<DadosCurso> dados = new ArrayList();
       open();
       try {
            ResultSet rs;
            PreparedStatement stmt = conn.prepareStatement("SELECT nomeCurso FROM atividadeparcial01.curso;");
            rs = stmt.executeQuery();
            
            while (rs.next()){
                DadosCurso dadosMySQL = new DadosCurso(rs.getString("nomeCurso"));
                dados.add(dadosMySQL);
            }
           
       } catch (SQLException ex) {
           System.out.println("DEU ERRO AQUI NO BUSCAR TABLEVIEW:" +ex);
       }finally{
       close();
       }
       return dados;
    }
    
}
