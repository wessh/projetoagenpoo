package Model.MySql;

import Model.Dados;
import Model.OrdemDados;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Arthur Reginaldo Ramos
 */
public class DadosMySQL extends MysqlDatabase implements OrdemDados{

    @Override
    public void adicionar(Dados dados) {
        open();
        try {
            System.out.println("Conecto"+dados.getNome());
            
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO atividadeparcial01.registro (NOME, CPF, CURSO) VALUES (?, ?, ?);");
            stmt.setString(1, dados.getNome());
            stmt.setString(2, dados.getCpf());
            stmt.setString(3, dados.getCurso());
            stmt.executeUpdate();
                        
       } catch (SQLException ex) {
        System.out.println("DEU ERRO AQUI NO Adicionar na TABLEVIEW:" +ex);
       }finally{
       close();
       }
        
        
    }

    @Override
    public void editar(Dados dados) {
        open();
            try {
            System.out.println("Editando: "+dados.getNome());
            
            PreparedStatement stmt = conn.prepareStatement("UPDATE atividadeparcial01.registro SET NOME=?, CPF=?, CURSO=? WHERE ID=?;");
            stmt.setString(1, dados.getNome());
            stmt.setString(2, dados.getCpf());
            stmt.setString(3, dados.getCurso());
            stmt.setInt(4, dados.getId());
            stmt.executeUpdate();
                        
       } catch (SQLException ex) {
        System.out.println("DEU ERRO AQUI NO BUSCAR TABLEVIEW:" +ex);
       }finally{
       close();
       }
    }
    
    
        @Override
    public List<Dados> allOpen() {
        ArrayList<Dados> dadosTabela = new ArrayList();
       open();
       try {
            ResultSet rs;
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM atividadeparcial01.registro;");
            rs = stmt.executeQuery();
            
            while (rs.next()){
                Dados dados = new Dados(rs.getInt("ID"),rs.getString("NOME"), rs.getString("CPF"),rs.getString("CURSO"));
                dadosTabela.add(dados);
            }
           
       } catch (SQLException ex) {
           System.out.println("DEU ERRO AQUI NO BUSCAR TABLEVIEW:" +ex);
       }finally{
       close();
       }
       return dadosTabela;
    
    }

    @Override
    public void excluir(Dados dados) {
      open();
        try {
            System.out.println(dados.getId());
            
            PreparedStatement stmt = conn.prepareStatement("DELETE FROM atividadeparcial01.registro WHERE ID=?;");
            stmt.setInt(1, dados.getId());
            stmt.executeUpdate();
            
        } catch (SQLException ex) { 
            System.out.println("DEU ERRO AQUI NO BUSCAR TABLEVIEW:" +ex);
       }finally{
       close();
       }
        
    }

}
